import { getAcceleration } from "../helpers/accelerationHelper"
import SpeedValidatorBase from "./speedValidatorBase"
import Notifications from "../helpers/notifications"

export default class AcceleratingSharplyValidator extends SpeedValidatorBase {
  speedsNeeded = 2;
  notification = Notifications.acceleratingSharply;
  periodTime_AS = 4;
  maxRPM = 2200;

  shouldShowAlert() {
    return this.rpms[this.speedsNeeded - 1] >= this.maxRPM && this.speeds[0] < 5 &&
      getAcceleration(this.speeds[0], this.speeds[1], this.timeLapse) > this.periodTime_AS;
  }
}
