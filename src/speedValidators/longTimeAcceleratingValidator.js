import { getDrivingMode } from "../helpers/drivingModeHelper"
import SpeedValidatorBase from "./speedValidatorBase"
import Notifications from "../helpers/notifications"

export default class LongTimeAcceleratingValidator extends SpeedValidatorBase {
  speedsNeeded = 5
  notification = Notifications.longTimeAccelerating
  acceleratingCount = 0
  accelerationCondition_LA = 5
  maxRPM = 2000
  minSpeed = 60

  shouldShowAlert() {
    let i = 0;
    do {
      let isAccelratingResult = getDrivingMode(this.speeds[i], this.speeds[i + 1])
      this.acceleratingCount = (isAccelratingResult === 1 && this.rpms[i] >= this.maxRPM
        && this.speeds[i] >= this.minSpeed) ? this.acceleratingCount += 1 : 0
      i++;
    }
    while (i + 1 < this.speedsNeeded);
    return (this.acceleratingCount > this.accelerationCondition_LA) ? true : false;
  }
}
