import SpeedValidatorBase from "./speedValidatorBase";
import Notifications from "../helpers/notifications";

export default class FrecuentlySopAndStartValidator extends SpeedValidatorBase {
  speedsNeeded = 60;
  notification = Notifications.frecuentlyStopAndStart;

  shouldShowAlert() {
    let cont = 0
    for (let i = 0; i < this.speeds.length; i++) {
      if (this.speeds[i] === 0 && (this.speeds[i + 1] && this.speeds[i + 1] > 0)) {
        cont += 1
      }
    }
    return cont >= 3
  }
}
