import SpeedValidatorBase from "./speedValidatorBase";
import Notifications from '../helpers/notifications';

export default class LongTimeIdlingValidator extends SpeedValidatorBase {
  speedsNeeded = 31;
  notification = Notifications.longTimeIdling;
  idlingInMotorTime_LI = 30;

  shouldShowAlert() {
    let idlingCount = 0;
    for (let i = 0; i < this.speeds.length; i++) {
      if (this.speeds[i] === 0) {
        idlingCount++;
      } else {
        idlingCount = 0;
      }
    }
    return idlingCount > this.idlingInMotorTime_LI;
  }
}
