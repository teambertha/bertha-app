import { getAcceleration } from "../helpers/accelerationHelper"
import SpeedValidatorBase from "./speedValidatorBase"
import Notfications from "../helpers/notifications"

export default class DeceleratingSharplyValidator extends SpeedValidatorBase {
  speedsNeeded = 2
  notification = Notfications.deceleratingSharply
  periodTime_AS = 9
  superiorLimit = 30

  shouldShowAlert() {
    return this.speeds[0] >= this.superiorLimit && getAcceleration(this.speeds[0], this.speeds[1], this.timeLapse) < -this.periodTime_AS
  }
}
