import SpeedValidatorBase from "./speedValidatorBase";
import Notifications from "../helpers/notifications";
import { getProductDrivingMode } from "../helpers/productDrivingModesHelper";

export default class BrakingModeratelyValidator extends SpeedValidatorBase {
  speedsNeeded = 6;
  notification = Notifications.brakingModerately;

  minAceleration() {
    let i = 0, validation = 0;
    do {
      (this.speeds[i + 1] - this.speeds[i]) >= -5 ? validation += 1 : 0;
      i++;
    } while (i <= this.speedsNeeded);
    return validation === 5;
  }

  shouldShowAlert() {
    const diff = this.speeds[this.speedsNeeded - 1] - this.speeds[0];
    return (-25 <= diff && diff <= -15) && this.minAceleration() && (31 < getProductDrivingMode(this.speeds) && getProductDrivingMode(this.speeds) < 49);
  }
}
