import PushNotification from 'react-native-push-notification'

export default class SpeedValidatorBase {
  speeds = []
  rpms = []
  speedsNeeded = 2
  timeLapse = 1
  notification = {}

  validate(speed, rpm) {
    this.speeds.push(speed)
    this.rpms.push(rpm)

    if (this.speeds.length < this.speedsNeeded) {
      return
    }

    if (this.shouldShowAlert()) {
      this.showAlert()
      this.speeds = []
      this.rpms = []
    }

    if (this.speeds.length == this.speedsNeeded) {
      this.speeds.shift()
      this.rpms.shift()
    }
  }

  shouldShowAlert() {
    throw Error("Must be implemented by subclass")
  }

  showAlert() {
    PushNotification.localNotification(this.notification)
    PushNotification.cancelLocalNotifications({ id: this.notification.id })
  }
}
