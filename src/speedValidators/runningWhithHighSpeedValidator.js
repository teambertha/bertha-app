import SpeedValidatorBase from "./speedValidatorBase";
import Notifications from "../helpers/notifications";
import { getAverage } from '../helpers/averageHelper';

export default class LongTimeRunningHighSpeedValidator extends SpeedValidatorBase {
  speedsNeeded = 5;
  notification = Notifications.runningWithHighSpeed;

  instantAcceleration() {
    let i = 0, validation = 0;
    do {
      (this.speeds[i + 1] - this.speeds[i]) <= 1 ? validation += 1 : 0;
      i++;
    } while (i < this.speedsNeeded);
    return validation === 4;
  }

  standardDeviation() {
    let sum = 0;
    const div = 1 / (this.speedsNeeded - 1);
    const avg = getAverage(this.speeds);
    this.speeds.forEach(speed => {
      sum += (speed - avg) * (speed - avg);
    });
    return (sum !== 0) ? Math.sqrt(sum * div) <= 1.5 : false;

  }

  shouldShowAlert() {
    return (getAverage(this.speeds) >= 90 && this.instantAcceleration() && this.standardDeviation()
      && (Math.abs(this.speeds[0] - this.speeds[this.speedsNeeded - 1]) <= 1));
  }

}
