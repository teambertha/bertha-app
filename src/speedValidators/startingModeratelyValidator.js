import SpeedValidatorBase from "./speedValidatorBase";
import Notifications from "../helpers/notifications";
import { getAcceleration } from "../helpers/accelerationHelper";
import { getProductDrivingMode } from '../helpers/productDrivingModesHelper';

export default class StartingModeratelyValidator extends SpeedValidatorBase {
  speedsNeeded = 5;
  notification = Notifications.startingModerately;


  maxAceleration() {
    let maxSpeed = false;
    for (let i = 1; i < this.speeds.length; i++) {
      maxSpeed = (getAcceleration(this.speeds[i - 1], this.speeds[i], 1) <= 4) ? true : false;
    }
    return maxSpeed;
  }

  shouldShowAlert() {
    return this.speeds[0] === 0 &&
      ((this.speeds[4] - this.speeds[0]) >= 10 && (this.speeds[4] - this.speeds[0] <= 20))
      && (getProductDrivingMode(this.speeds) === 1 || getProductDrivingMode(this.speeds) === 3)
      && this.maxAceleration() && this.rpms[this.speedsNeeded - 1] < 2500;
  }
}
