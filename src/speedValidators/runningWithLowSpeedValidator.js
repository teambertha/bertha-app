import SpeedValidatorBase from "./speedValidatorBase";
import Notifications from '../helpers/notifications';
import { getAverage } from "../helpers/averageHelper";

export default class LongTimeRunninWithLowSpeedValidator extends SpeedValidatorBase {
  speedsNeeded = 60;
  notification = Notifications.longTimeLowSpeed;
  averageSpeed_LS = 20;

  noIdlingValidator(){
    let isValid;
    this.speeds.forEach(speed => {
      isValid = (this.speeds[speed] !== 0) ? true : false;
    });
    return isValid;
  }

  shouldShowAlert() {
    const speedAverage = getAverage(this.speeds);
    const isInLowSpeed = (speedAverage <= this.averageSpeed_LS) ? true : false;
    return isInLowSpeed && this.noIdlingValidator();
  }
}
