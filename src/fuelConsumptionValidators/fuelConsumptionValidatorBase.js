import PushNotification from 'react-native-push-notification'

export default class FuelConsumptionValidatorBase {
  fuelConsumptions = []
  speeds = []
  elementsNeeded = 2
  timeLapse = 1
  notification = {}

  validate(speed, fuelConsumption) {
    this.fuelConsumptions.push(fuelConsumption)
    this.speeds.push(speed)

    if ((this.fuelConsumptions.length < this.elementsNeeded)
      || (this.speeds.length < this.elementsNeeded)) {
      return
    }

    if (this.fuelConsumptions.length == this.elementsNeeded) {
      this.fuelConsumptions.shift()
    }

    if (this.speeds.length == this.elementsNeeded) {
      this.speeds.shift()
    }

    if (this.shouldShowAlert().id === '10' || this.shouldShowAlert().id === '11'
      || this.shouldShowAlert().id === '12') {
      this.showAlert()
      this.speeds = []
      this.fuelConsumptions = []
    }
  }

  shouldShowAlert() {
    throw Error("Must be implemented by subclass")
  }

  showAlert() {
    PushNotification.localNotification(this.notification)
    PushNotification.cancelLocalNotifications({ id: this.notification.id })
  }
}
