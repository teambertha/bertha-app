import FuelConsumptionValidatorBase from "./fuelConsumptionValidatorBase";
import { FPHCalculator } from "../helpers/FPHHelper";
import Notifications from "../helpers/notifications";

export default class ScoreValidator extends FuelConsumptionValidatorBase {
  elementsNeeded = 120

  scoreCalculator() {
    score = 40 + (1 - ((parseInt(FPHCalculator(this.speeds, this.fuelConsumptions)) - 6) / (14 - 6))) * 60 //FPh esta entre [6, 14]

    if (score <= 60) {
      this.notification = Notifications.redScore //Red
    } else if ((score > 60) && (score <= 85)) {
      this.notification = Notifications.yellowScore //Yellow
    } else {
      this.notification = Notifications.greenScore //Green
    }
    return this.notification
  }

  shouldShowAlert() {
    return this.scoreCalculator()
  }
}
