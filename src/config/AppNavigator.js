import { createBottomTabNavigator } from 'react-navigation'
import React from 'react'
import { Image } from 'react-native'
import {
  Drive, DriveDisable,
  Log, LogDisable,
  Challenge, ChallengeDisable,
  Fuel, FuelDisable,
  Profile, ProfileDisable
} from '../assets/images'

import NotImplement from '../components/pages/NotImplement'
import StartDriveView from '../components/pages/StartDriveView'
import ShowMapView from '../components/pages/ShowMapView'

const AppNavigator = createBottomTabNavigator(
  {
    Viajes: { screen: ShowMapView },
    Combustible: { screen: NotImplement },
    Navegar: { screen: StartDriveView },
    Desafios: { screen: NotImplement },
    Perfil: { screen: NotImplement },
  },
  {
    initialRouteName: 'Navegar',
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state
        let iconName
        if (routeName === 'Navegar') {
          iconName = focused ? Drive : DriveDisable
        } else if (routeName === 'Viajes') {
          iconName = focused ? Log : LogDisable
        } else if (routeName === 'Combustible') {
          iconName = focused ? Fuel : FuelDisable
        } else if (routeName === 'Desafios') {
          iconName = focused ? Challenge : ChallengeDisable
        } else if (routeName === 'Perfil') {
          iconName = focused ? Profile : ProfileDisable
        }

        return <Image style={{ width: 24, height: 24 }}
          source={iconName} />
      },
    }),
    tabBarOptions: {
      activeTintColor: 'green',
      inactiveTintColor: 'gray',
    },
  })

export default AppNavigator