import PushNotification from 'react-native-push-notification'
var Buffer = require('buffer/').Buffer

export function downHillNotification() {
    PushNotification.localNotification({
        /* Android Only Properties */
        id: '0',
        autoCancel: true,
        largeIcon: 'ic_launcher',
        smallIcon: 'ic_notification',
        bigText: '¡Cuesta Abajo!',
        subText: '¡Se aproxima una cuesta abajo!',
        color: 'black',
        vibrate: true,
        vibration: 300,
        tag: 'DownHill',
        group: 'Bertha',
        ongoing: false,
        priority: 'high',
        visibility: 'private',
        importance: 'high',
        /* iOS and Android properties */
        title: 'Cuesta Abajo',
        message: '¡Reduce la aceleración!',
        playSound: false,
        soundName: 'default',
    })
    PushNotification.cancelLocalNotifications({ id: '0' })
}

export function upHillNotification() {
    PushNotification.localNotification({
        /* Android Only Properties */
        id: '1',
        autoCancel: true,
        largeIcon: 'ic_launcher',
        smallIcon: 'ic_notification',
        bigText: '¡Cuesta Arriba!',
        subText: '¡Se aproxima una cuesta arriba!',
        color: 'black',
        vibrate: true,
        vibration: 300,
        tag: 'UpHill',
        group: 'Bertha',
        ongoing: false,
        priority: 'high',
        visibility: 'private',
        importance: 'high',
        /* iOS and Android properties */
        title: 'Cuesta Arriba',
        message: '¡Aceleración de escalera y crucero!',
        playSound: false,
        soundName: 'default',
    })
    PushNotification.cancelLocalNotifications({ id: '1' })
}

export function asciiToB64(value) {
    return Buffer.from(value.concat('\r')).toString('base64')
}

export function b64ToAscii(value) {
    return Buffer.from(value, 'base64').toString('ascii')
}

export function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}