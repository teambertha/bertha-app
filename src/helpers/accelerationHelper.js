export function getAcceleration(initialSpeed, finalSpeed, timeLapse) {
  initialSpeed = initialSpeed
  finalSpeed = finalSpeed
  return (finalSpeed - initialSpeed) / timeLapse
}