export const FPHCalculator = (speeds, fuelConsumptions) => {
  let speedsSum = 0
  let fuelConsumptionsSum = 0

  speeds.forEach(speed => {
    speedsSum += speed
  })

  fuelConsumptions.forEach(fuelConsumption => {
    fuelConsumptionsSum += fuelConsumption
  })

  return (speedsSum !== 0) ? ((fuelConsumptionsSum / 3600) / (speedsSum / 3600)) * 100 : 0
}
