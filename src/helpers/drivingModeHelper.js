export function getDrivingMode(initialSpeed, finalSpeed) {
  mode = 1
  if ((finalSpeed - initialSpeed) === 0) {
    mode = 3
  }
  else if (finalSpeed === 0 && initialSpeed === 0) {
    mode = 4
  }
  else ((finalSpeed - initialSpeed) > 0) ? mode = 1 : mode = 2

  return mode
}
