import { getDrivingMode } from './drivingModeHelper';

export function getProductDrivingMode(speeds) {
  let product = 1;
  for (let i = 1; i < speeds.length; i++) {
    product *= getDrivingMode(speeds[i - 1], speeds[i]);
  }
  return product;
}
