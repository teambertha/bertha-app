const coordinateDistance = 10

export function lookForMidPoints(coordinates) {
  let newPath = []
  newPath.push(coordinates[0])
  for (let index = 1; index < coordinates.length; index++) {
    newPath.push(coordinates[index])
    let distance = this.calculateDistance(
      coordinates[index].latitude,
      coordinates[index].longitude,
      coordinates[index - 1].latitude,
      coordinates[index - 1].longitude
    )
    if (distance > coordinateDistance) {
      newPath = newPath.concat(this.addMidPoints(distance, coordinates[index], coordinates[index - 1]))
    }
  }
  return newPath
}

addMidPoints = (distance, currentCoordinate, previousCoordinate) => {
  let newPoints = []
  let pointsToAdd = Math.ceil(distance / coordinateDistance)
  let latDiff = currentCoordinate.latitude - previousCoordinate.latitude
  let lngDiff = currentCoordinate.longitude - previousCoordinate.longitude
  for (let l = 1; l < pointsToAdd; l++) {
    let lat = previousCoordinate.latitude + ((latDiff / pointsToAdd) * l)
    let lng = previousCoordinate.longitude + ((lngDiff / pointsToAdd) * l)
    newPoints.push({ latitude: lat, longitude: lng })
  }
  return newPoints
}

calculateDistance = (startLatitude, startLongitude, endLatitude, endLongitude) => {
  let earthRadius = 6371e3 // metres
  let radianLatitude = this.toRadians(startLatitude)
  let radianLongitude = this.toRadians(endLatitude)
  let latitudeDiff = this.toRadians(endLatitude - startLatitude)
  let longitudeDiff = this.toRadians(endLongitude - startLongitude)
  let haversine = Math.sin(latitudeDiff / 2) * Math.sin(latitudeDiff / 2) +
    Math.cos(radianLatitude) * Math.cos(radianLongitude) *
    Math.sin(longitudeDiff / 2) * Math.sin(longitudeDiff / 2)
  let c = 2 * Math.atan2(Math.sqrt(haversine), Math.sqrt(1 - haversine))
  return earthRadius * c
}

toRadians = (degrees) => {
  return degrees * Math.PI / 180
}

export function isMultiple(a, b) {
  return (a != 0 && a % (b - 1) === 0)
}