export function getAverage(speeds) {
  let sum = 0
  speeds.forEach(element => {
    sum += element
  });
  return sum / speeds.length
}
