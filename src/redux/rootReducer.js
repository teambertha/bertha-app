import { combineReducers } from 'redux'
import maps from './modules/maps'
import obd from './modules/obd'

const rootReducer = combineReducers({
  maps,
  obd,
})

export default rootReducer