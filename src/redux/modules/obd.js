import { handleActions, createAction } from 'redux-actions'

// Actions
export const SET_CURRENT_DATA = 'bertha-app/obd/SET_CURRENT_DATA'

export const getInitialState = {
  data: {
    FUEL: 0,
    RPM: 0,
    SPEED: 0,
    ENGINEFUELRATE: 0
  }
}

// Reducer
const obdReducer = handleActions(
  {
    [SET_CURRENT_DATA](state, action) {
      const { data } = action.payload
      return {
        ...state, data
      }
    },
  },
  getInitialState,
)


// Action Creators
export default obdReducer

// Action Creators
export const setCurrentData = ({ FUEL, RPM, SPEED, ENGINEFUELRATE }) => {
  return createAction(SET_CURRENT_DATA)({
    data: {
      FUEL,
      RPM,
      SPEED,
      ENGINEFUELRATE
    }
  })
}
