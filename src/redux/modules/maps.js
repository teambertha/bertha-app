import { handleActions, createAction } from 'redux-actions'

// Actions
export const SET_CURRENT_POSITION = 'bertha-app/maps/SET_CURRENT_POSITION'
export const SET_MARKER_LIST = 'bertha-app/maps/SET_MARKER_LIST'
export const SET_DESTINATION = 'bertha-app/maps/SET_DESTINATION'
export const SET_ROUTE_COORDINATES = 'bertha-app/maps/SET_ROUTE_COORDINATES'

export const getInitialState = {
  currentPosition: null,
  markerList: [],
  destination: null,
  routeCoordinates: [{ latitude:0, longitude:0 }],
}

// Reducer
const mapReducer = handleActions(
  {
    [SET_CURRENT_POSITION](state, action) {
      const { currentPosition } = action.payload
      return {
        ...state, currentPosition
      }
    },
    [SET_MARKER_LIST](state, action) {
      const { markerList } = action.payload
      return {
        ...state, markerList
      }
    },
    [SET_DESTINATION](state, action) {
      const { destination } = action.payload
      return {
        ...state, destination
      }
    },
    [SET_ROUTE_COORDINATES](state, action) {
      const { routeCoordinates } = action.payload
      return {
        ...state, routeCoordinates
      }
    },
  },
  getInitialState,
)


// Action Creators
export default mapReducer

// Action Creators
export const setCurrentPosition = (latitude, longitude) => {
  return createAction(SET_CURRENT_POSITION)({
    currentPosition: {
      latitude,
      longitude
    }
  })
}

export const setMarkerList = (markerList) => {
  return createAction(SET_MARKER_LIST)({
    markerList
  })
}

export const setDestination = (destination) => {
  return createAction(SET_DESTINATION)({
    destination
  })
}
export const setRouteCoordinates = (routeCoordinates) => {
  return createAction(SET_ROUTE_COORDINATES)({
    routeCoordinates
  })
}