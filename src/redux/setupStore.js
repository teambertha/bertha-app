import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import rootReducer from './rootReducer'

export default function setupStore(initialState = {}) {
  let middlewares = []
  if (process.env.NODE_ENV === 'development') {
    const logger = createLogger({ collapsed: true })
    middlewares.push(logger)
  }
  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middlewares)
  )
  return store
}