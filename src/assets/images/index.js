import UpHillImage from './icons/UpHillNotification.png'
import DownHillImage from './icons/DownHillNotification.png'
import Logo from './icons/Logo.png'
import Drive from './icons/Drive.png'
import DriveDisable from './icons/DriveDisable.png'
import Challenge from './icons/Challenge.png'
import ChallengeDisable from './icons/ChallengeDisable.png'
import Fuel from './icons/Fuel.png'
import FuelDisable from './icons/FuelDisable.png'
import Log from './icons/Log.png'
import LogDisable from './icons/LogDisable.png'
import Profile from './icons/Profile.png'
import ProfileDisable from './icons/ProfileDisable.png'

export {
    UpHillImage,
    DownHillImage,
    Logo,
    Drive,
    DriveDisable,
    Challenge,
    ChallengeDisable,
    Fuel,
    FuelDisable,
    Log,
    LogDisable,
    Profile,
    ProfileDisable
}