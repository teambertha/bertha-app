import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      borderStyle: 'solid',
      borderRadius: 5,
      borderWidth: 5,
      borderColor: '#d6d7da',
      margin: 25,
    },
    title: {
      
    }
  })