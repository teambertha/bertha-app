import { StyleSheet } from 'react-native'


export const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    view: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
    },
    textInput: {
        marginTop: 20,
        marginRight: 20,
        marginLeft: 20,
        width: '80%',
        borderRadius: 10,
        backgroundColor: '#FFFFFF',
        paddingLeft: 20,
    },
    touchable: {
        alignItems: 'center',
        backgroundColor: 'green',
        padding: 10,
        borderRadius: 50,
        width: 100,
    },
    buttonText: {
        color: 'white',
    }
})