import React, { Component } from 'react'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
import { Marker } from 'react-native-maps'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { API_KEY } from 'react-native-dotenv'
import MapViewDirections from 'react-native-maps-directions'
import { lookForMidPoints, isMultiple } from '../../helpers/Geometry'

class Map extends Component {
  constructor(props) {
    super(props)
    this.state = {
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0,
      },
      error: null,
      destination: null,
      elevation: [],
      coordinates: null,
    }
    this.watchId = -1
  }

  componentDidMount() {
    this.initWatcher()
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId)
  }

  initWatcher = async () => {
    this.watchId = await navigator.geolocation.watchPosition(
      (position) => {
        const { latitude, longitude } = position.coords
        this.setState(() => ({
          region: {
            ...this.state.region,
            latitude,
            longitude,
          },
          error: null,
        }))
      },
      (error) => {
        alert(error.message)
        this.setState({ error: error.message })
      },
      { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 },
    )
    this.getDelta(1000)
  }

  getDelta = (distance) => {
    const { latitude } = this.state.region
    const oneDegreeOfLatitudeInMeters = 111.32 * 1000
    const latitudeDelta = distance / oneDegreeOfLatitudeInMeters
    const longitudeDelta = distance / (oneDegreeOfLatitudeInMeters * Math.cos(latitude * (Math.PI / 180)))
    this.setState(() => ({
      region: {
        ...this.state.region,
        latitudeDelta,
        longitudeDelta
      }
    }))
  }

  getChunkElevation = (url) => {
    fetch(url)
      .then((response) => response.json())
      .then((elevation) => {
        this.setState(() => ({
          elevation: [...this.state.elevation, ...Array.from(elevation.results, value => value.elevation)]
        }))
      })
      .catch((error) => {
        alert('No se pudo obtener la altitud de la ruta')
      })
  }

  getCoordinatesElevation = () => {
    const coordinatesPerRequest = 200
    const { coordinates } = this.state
    let pointsPerRequest = coordinatesPerRequest
    let path = ''
    coordinates.forEach((coordinate, i) => {
      path = `${path}${coordinate.latitude},${coordinate.longitude}|`
      if (isMultiple(i, pointsPerRequest) || i === coordinates.length - 1) {
        path = path.slice(0, -1)
        this.getChunkElevation(`https://maps.googleapis.com/maps/api/elevation/json?locations=${path}&key=${API_KEY}`)
        path = ''
      }
    })
  }

  render() {
    const { latitude, longitude } = this.state.region
    const { destination } = this.props
    return (
      <View style={styles.container}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          region={this.state.region}
        >
          <Marker coordinate={{ latitude, longitude }} />
          <MapViewDirections
            origin={{ latitude, longitude }}
            destination={destination}
            strokeWidth={3}
            apikey={API_KEY}
            onReady={(result) => {
              let preciseCoordinates = lookForMidPoints(result.coordinates)
              this.setState(() => ({
                coordinates: preciseCoordinates,
                elevation: []
              }))
              this.getCoordinatesElevation()
            }}
          />
        </MapView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: { ...StyleSheet.absoluteFillObject },
  map: { ...StyleSheet.absoluteFillObject }
})

function mapStateToProps(state) {
  return {
    destination: state.maps.destination,
  }
}

export default connect(mapStateToProps, null)(Map)