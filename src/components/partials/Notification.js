import React from 'react'
import PushNotification from 'react-native-push-notification'

class PushNotificationController extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        PushNotification.configure({

            onNotification: function (notification) {
                console.log('NOTIFICATION:', notification)
            },
            popInitialNotification: false,

        });
    }

    render() {
        return null;
    }
}

export default PushNotificationController