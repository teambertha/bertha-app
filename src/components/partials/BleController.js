import React from 'react'
import { Platform } from 'react-native'
import { BleManager } from 'react-native-ble-plx'
import { asciiToB64, b64ToAscii, sleep } from '../../helpers/helpers'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { setCurrentData } from '../../redux/modules/obd'

import { CODES, DEVICE_INFO, TIME_SLEEP } from '../constants/OBDCodes'

class BleController extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      data: {
        FUEL: 0,
        RPM: 0,
        SPEED: 0,
        ENGINEFUELRATE: 0
      },
      monitorData: '',
      device: null,
      currentCode: '',
      info: '',
    }

    this.manager = new BleManager()
  }

  componentDidMount() {
    if (Platform.OS === 'ios') {
      this.manager.onStateChange((state) => {
        if (state === 'PoweredOn') this.scanAndConnect()
      })
    } else {
      this.scanAndConnect()
    }
  }

  componentWillUnmount() {
    if (this.timer != null) {
      clearInterval(this.timer)
      this.timer = null
    }
  }

  async updateData(value) {

    this.setState((prevState) => ({
      monitorData: prevState.monitorData + b64ToAscii(value)
    }))

    if (this.state.monitorData.includes('>')) {
      this.setState((prevState) => ({
        monitorData: prevState.monitorData.replace('\n', '').replace('>', '')
      }))
      this.moveData()
    }
  }

  moveData() {
    const { monitorData, currentCode, data } = this.state
    let { FUEL, RPM, SPEED, ENGINEFUELRATE } = data
    const { setCurrentData } = this.props

    this.setState(() => ({
      info: currentCode
    }))
    if (currentCode === 'FUEL') {
      FUEL = this.dataToFuel(monitorData)
      this.setState({ currentCode: 'RPM' })
      this.sendCode(CODES.RPM)

    } else if (currentCode === 'RPM') {
      RPM = this.dataToRPM(monitorData)
      this.setState({ currentCode: 'SPEED' })
      this.sendCode(CODES.SPEED)

    } else if (currentCode === 'SPEED') {
      SPEED = this.dataToSpeed(monitorData)
      this.setState({ currentCode: 'ENGINEFUELRATE' })
      this.sendCode(CODES.ENGINEFUELRATE)

    } else if (currentCode === 'ENGINEFUELRATE') {
      ENGINEFUELRATE = this.dataToEngineFuelRate(monitorData)
      this.setState({ currentCode: 'FUEL' })
      this.sendCode(CODES.FUEL)
    
    } else {
      this.setState({ currentCode: 'FUEL' })
      this.sendCode(CODES.FUEL)
      return
    }

    this.setState(() => ({
      data: {
        FUEL: FUEL,
        RPM: RPM,
        SPEED: SPEED,
        ENGINEFUELRATE: ENGINEFUELRATE
      }
    }))

    setCurrentData({
      FUEL: FUEL,
      RPM: RPM,
      SPEED: SPEED,
      ENGINEFUELRATE: ENGINEFUELRATE
    })

    this.clearData()
  }

  clearData() {
    this.setState({ monitorData: '' })
  }

  scanAndConnect = () => {
    this.manager.startDeviceScan(null, null, (error, device) => {
      if (error) {
        return
      }
      if (device.id === DEVICE_INFO.ID || device.name === DEVICE_INFO.NAME) {
        this.manager.stopDeviceScan()
        device.connect()
          .then((device) => {
            return device.discoverAllServicesAndCharacteristics()
          })
          .then((device) => {
            return this.setupNotifications(device)
          })
          .then((device) => {
            this.setState({ device: device })
            alert('Device setted: ' + this.state.device.name)

            this.setState({ currentCode: 'FUEL' })
            this.sendCode(CODES.FUEL)
          }, (error) => {
            alert(error)
          })
      }
    })
  }

  setupNotifications(device) {
    const { serviceUUID, readCharacteristicUUID, writeCharacteristicUUID, ATZ, ATH, ATSP, FUEL } = CODES

    //Configuring OBD
    sleep(TIME_SLEEP.CONFIG_TIME)
    device.writeCharacteristicWithoutResponseForService(serviceUUID, writeCharacteristicUUID, asciiToB64(ATZ))
    sleep(TIME_SLEEP.CONFIG_TIME)
    device.writeCharacteristicWithoutResponseForService(serviceUUID, writeCharacteristicUUID, asciiToB64(ATH))
    sleep(TIME_SLEEP.CONFIG_TIME)
    device.writeCharacteristicWithoutResponseForService(serviceUUID, writeCharacteristicUUID, asciiToB64(ATSP))

    sleep(TIME_SLEEP.CONFIG_TIME)
    this.setState({ currentCode: 'FUEL' })
    device.writeCharacteristicWithoutResponseForService(serviceUUID, writeCharacteristicUUID, FUEL)

    sleep(TIME_SLEEP.CONFIG_TIME)
    //setting up reading listener
    device.monitorCharacteristicForService(serviceUUID, readCharacteristicUUID, async (error, characteristic) => {
      if (error) {
        return
      }
      this.updateData(characteristic.value)
    })

    return device
  }

  sendCode(code) {
    const { device } = this.state
    const { serviceUUID, writeCharacteristicUUID } = CODES

    sleep(TIME_SLEEP.CODE_TIME)
    device.writeCharacteristicWithoutResponseForService(serviceUUID, writeCharacteristicUUID, asciiToB64(code))
  }

  dataToFuel(value) {
    const { FUEL_SEPARATOR } = CODES
    value = value.split(FUEL_SEPARATOR)
    value = parseInt(value[1], 16) / 255 * 100
    return Math.round(value)
  }

  dataToRPM(value) {
    const { RPM_SEPARATOR } = CODES
    value = value.split(RPM_SEPARATOR)
    value = (value[1]).trim().replace(' ', '')
    value = parseInt(value, 16) / 4
    return value
  }

  dataToSpeed(value) {
    const { SPEED_SEPARATOR } = CODES
    value = value.split(SPEED_SEPARATOR)
    value = parseInt(value[1], 16)
    return value
  }

  dataToEngineFuelRate(value) {
    const { ENGINFUELRATE_SEPARATOR } = CODES
    value = value.split(ENGINFUELRATE_SEPARATOR)
    value = (value[1]).trim().replace(' ', '')
    value = (parseInt(value, 16) - 125)
    return value
  }

  render() {
    return null
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    setCurrentData,
  }, dispatch)
}

function mapStateToProps(state) {
  return {
    data: state.obd.data,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BleController)
