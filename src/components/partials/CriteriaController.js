import React from 'react'
import { connect } from 'react-redux'

import { View, Text, TouchableOpacity } from 'react-native'
import { styles } from '../../assets/styles/ShowMapStyle'

import PushNotificationController from '../partials/Notification'
import SpeedValidator from "../../validators/speedValidator"
import FuelConsumptionValidator from '../../validators/fuelConsumptionValidator';

const ONE_SECOND = 1000 // millseconds

class CriteriaController extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      time: 0,
    }

    this.speedValidationTimer = null
    this.speedValidator = new SpeedValidator()
    this.fuelConsumptionValidationTimer = null
    this.fuelConsumptionValidator = new FuelConsumptionValidator()

  }

  startDrive = async () => {
    if (!this.timer) {
      this.timer = setInterval(
        () => {
          this.setState((prevState) => ({ time: prevState.time + 1 }))
        }, ONE_SECOND)

      this.speedValidationTimer = setInterval(() => {
        this.speedValidator.speedAdded(this.props.obdData.SPEED, this.props.obdData.RPM)
      }, ONE_SECOND)

      this.fuelConsumptionValidationTimer = setInterval(() => {
        this.fuelConsumptionValidator.fuelConsumptionAdded(this.props.obdData.SPEED, this.props.obdData.ENGINEFUELRATE)
      }, ONE_SECOND)
    }
  }

  stopDrive = async () => {
    this.setState({ time: 0 })

    clearInterval(this.timer)
    this.timer = null

    clearInterval(this.speedValidationTimer)
    this.speedValidationTimer = null

    clearInterval(this.fuelConsumptionValidationTimer)
    this.fuelConsumptionValidationTimer = null

    this.clearValidators()
  }

  clearValidators = () => {
    this.speedValidator.validators.forEach(validator => {
      validator.speeds = []
      validator.rpms = []
    })

    this.fuelConsumptionValidator.validators.forEach(validator => {
      validator.speeds = []
      validator.fuelConsumptions = []
    })
  }

  render() {
    const { time } = this.state
    return (
      <View>
        <Text>Current Data</Text>
        <Text>Seconds: {time}</Text>
        <TouchableOpacity onPress={this.startDrive} style={styles.touchable}>
          <Text style={styles.buttonText}>Ir</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={this.stopDrive} style={styles.touchable}>
          <Text style={styles.buttonText}>Terminar</Text>
        </TouchableOpacity>

        <PushNotificationController />
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    obdData: state.obd.data,
  }
}

export default connect(mapStateToProps, null)(CriteriaController)
