import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { styles } from '../../assets/styles/NotImplement'
import { connect } from 'react-redux'

class NotImplement extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text> Estamos trabajando en esta funcionalidad :) </Text>
      </View>
    )
  }
}

export default connect(null, null)(NotImplement)
