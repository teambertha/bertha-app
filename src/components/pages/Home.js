import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { styles } from '../../assets/styles/HomeStyle'
import { connect } from 'react-redux'
import BleController from '../partials/BleController'

class Home extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      data: null
    }
  }

  static getDerivedStateFromProps(props, state) {
    const { data } = props
    return {
      data,
    }
  }

  render() {
    const { data } = this.state
    return (
      <View style={styles.container}>
        <Text>FUEL: {data ? data.FUEL : 'Vacio'}</Text>
        <Text>SPEED: {data ? data.SPEED : 'Vacio'}</Text>
        <Text>RPM: {data ? data.RPM : 'Vacio'}</Text>
        <BleController />
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    data: state.obd.data,
  }
}

export default connect(mapStateToProps, null)(Home)
