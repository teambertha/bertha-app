import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { styles } from '../../assets/styles/StartDriveStyle'
import { connect } from 'react-redux'
import BleController from '../partials/BleController'
import CriteriaController from '../partials/CriteriaController'

class StartDrive extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { data } = this.props
    return (
      <View style={styles.container}>

        <View>
          <Text> Start New Drive </Text>
        </View>
        <Text>
          ----------------------------
        </Text>
        <Text>Obd real information</Text>
        <Text>FUEL: {data ? data.FUEL : 'Vacio'}</Text>
        <Text>SPEED: {data ? data.SPEED : 'Vacio'}</Text>
        <Text>RPM: {data ? data.RPM : 'Vacio'}</Text>
        <Text>Engine Fuel Rate: {data ? data.ENGINEFUELRATE : 'Vacio'}</Text>
        <BleController />
        <Text>
          -----------------------------
        </Text>
        <CriteriaController />
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    data: state.obd.data,
  }
}

export default connect(mapStateToProps, null)(StartDrive)
