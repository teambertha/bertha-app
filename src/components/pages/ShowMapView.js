import React, { Component } from 'react'
import { TextInput, View, TouchableOpacity, Text } from 'react-native'
import Map from '../partials/Map'
import { PermissionsAndroid } from 'react-native'
import { connect } from 'react-redux'
import { setDestination } from '../../redux/modules/maps'
import { bindActionCreators } from 'redux'
import { styles } from '../../assets/styles/ShowMapStyle'

export async function requestCameraPermission() {
  try {
    const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {})
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    } else {
      alert('Permisos de localización denegados')
    }
  } catch (err) {
  }
}

class ShowMapView extends Component {
  constructor(props) {
    super(props)

    this.state = {
      destination: '',
    }
  }

  componentDidMount() {
    requestCameraPermission()
  }

  sendDestination = () => {
    const { setDestination } = this.props
    const { destination } = this.state
    setDestination(destination)
  }

  updateState = (field, value) => {
    this.setState(() => ({
      [field]: value
    }))
  }

  render() {
    const { destination } = this.state
    return (
      <View style={styles.container}>
        <Map style={styles.map}/>
        <View style={styles.view}>
          <TextInput
            onChangeText={(destination) => this.updateState('destination', destination )}
            value={destination}
            style={styles.textInput}
            placeholder='Ingrese el destino'
            placeholderTextColor='gray'
          />
          <TouchableOpacity onPress={this.sendDestination} style={styles.touchable}>
            <Text style={styles.buttonText}>Ir</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    setDestination,
  }, dispatch)
}

export default connect(null, mapDispatchToProps)(ShowMapView)