import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, AppState } from 'react-native'
import PushNotificationController from '../partials/Notification'
import { downHillNotification, upHillNotification } from '../../helpers/helpers'

export default class SendNotificaction extends React.Component {
  constructor(props) {
    super(props)


  }
  
  downhill = () => {
    downHillNotification()
  }

  uphill = () => {
    upHillNotification()
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.uphill()}>
          <Text>Send Uphill Notificaction</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.downhill()}>
          <Text>Send Downhill Notificaction</Text>
        </TouchableOpacity>
        <PushNotificationController />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});