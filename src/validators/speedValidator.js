import AcceleratingSharplyValidator from "../speedValidators/acceleratingSharplyValidator";
import DeceleratingSharplyValidator from "../speedValidators/deceleratingSharplyValidator"
import LongTimeIdlingValidator from "../speedValidators/longTimeIdlingValidator";
import LongTimeAcceleratingValidator from "../speedValidators/longTimeAcceleratingValidator";
import LongTimeRunninWithLowSpeedValidator from "../speedValidators/runningWithLowSpeedValidator";
import LongTimeRunningHighSpeedValidator from "../speedValidators/runningWhithHighSpeedValidator";
import StartingModeratelyValidator from "../speedValidators/startingModeratelyValidator";
import BrakingModeratelyValidator from "../speedValidators/brakingModeratelyValidator";
import FrecuentlySopAndStartValidator from "../speedValidators/frecuentlyStopAndStartValidator";

export default class SpeedValidator {
  validators = [
    new AcceleratingSharplyValidator(),
    new DeceleratingSharplyValidator(),
    new LongTimeIdlingValidator(),
    new LongTimeAcceleratingValidator(),
    new LongTimeRunninWithLowSpeedValidator(),
    new LongTimeRunningHighSpeedValidator(),
    new StartingModeratelyValidator(),
    new BrakingModeratelyValidator(),
    new FrecuentlySopAndStartValidator()
  ]

  speedAdded(speed, rpm) {
    this.validators.forEach((validator) => {
      validator.validate(speed, rpm)
    })
  }
}
