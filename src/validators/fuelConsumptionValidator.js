import ScoreValidator from "../fuelConsumptionValidators/ScoresValidator";

export default class FuelConsumptionValidator {
  validators = [
    new ScoreValidator()
  ]

  fuelConsumptionAdded(speed, fuelConsumption) {
    this.validators.forEach((validator) => {
      validator.validate(speed, fuelConsumption)
    })
  }
}
