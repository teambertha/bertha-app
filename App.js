import React from 'react'
import AppNavigator from './src/config/AppNavigator'
import setupStore from './src/redux/setupStore'
import { Provider } from 'react-redux'

const store = setupStore()

const App = () => (
  <Provider store={store}>
    <AppNavigator />
  </Provider>
)

export default App