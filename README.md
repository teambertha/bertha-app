## Setting up

1. Create a **.env** in you project's root
2. Add an **API_KEY** value which must contains you Google API Key : API_KEY=AIzaSyBtc5cAD-wq2ra8K1WpOwjDp8rluH_rF3E
3. Install **npm** and **node**
4. In terminal : node -v it should shows the node version
5. In terminal : rpm -v and its shows the npm version
6. Install **Android Studio**(*https://developer.android.com/studio/?hl=es-419*).
On Android Studio toolbar  select Tools then Android > SDK Manager.
In the Default Settings dialog box, click these tabs to install Android SDK platform packages and developer tools.
* SDK Platforms: Select the latest Android SDK package.
* SDK Tools: Select these Android SDK tools:
    * Android SDK Build-Tools
    * Android SDK Platform-Tools
    * Android SDK Tools
    * NDK
    * Android Support Repository
    * Google Repository
7. Set **ANDROID_HOME**.
8. Check if **ANDROID_HOME** was set with: *echo $ANDROID_HOME* in terminal
9. Install Java (*https://www.oracle.com/technetwork/java/javase/downloads/index.html*) 
10. Set **JAVA_HOME**
11. Check if **JAVA_HOME** was set with: *echo $JAVA_HOME* in terminal
12. Install **yarn** (*https://yarnpkg.com/en/docs/install#mac-stable*)
13. Install **React** and **react-native**
14. Execute *sh run.sh* in Freematics Folder, then connect the usb devise and active the bluetooth in  your phone 
15. Wait until your device and the emulator get synchronize 
16. In terminal execute *yarn install* located in bertha-app folder
17. The execute *yarn start*. 
18. Run your project with **react-native run-android** 